const test = require('blue-tape')
const {requestDestination} = require('..')
const {_requestDestination} = require('../_requestDestination')

const fixtures = [
  [['js'], 'script'],
  [['css'], 'style'],
  [['svg', 'png', 'jpg', 'jpeg', 'webp', 'ico'], 'image'],
  [['woff', 'woff2', 'ttf', 'eot', 'otf'], 'font'],
  [['mp4', 'webm'], 'video'],
  [['mp3', 'aac', 'wav', 'flac'], 'audio'],
  [['vtt'], 'track'],
  [['html', 'txt', 'json'], ''] // or should this be 'document'?
]

test('Mixed-case lookup', async (t) => {
  for (const [extensions, expected] of fixtures) {
    for (const extension of extensions) {
      const cases = [
        `/foo/bar.${extension}`,
        `/foo/bar.${extension.toUpperCase()}`
      ]
      for (const given of cases) {
        const actual = requestDestination(given)
        t.is(actual, expected)
        const _actual = _requestDestination(given)
        t.is(_actual, expected)
      }
    }
  }
})
