const test = require('blue-tape')
const {Suite} = require('benchmark')
const {requestDestination} = require('..')
const {_requestDestination} = require('../_requestDestination')

test('benchmark', (t) => {
  return new Promise((resolve) => {
    const benchmark = new Suite()
      .add('Pretty', () => {
        _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.js')
        _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.css')
        _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.woff')
        _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.svg')
      })
      .add('Ugly', () => {
        requestDestination('/foo/bar/bla/bla/blaaaaaaaa.js')
        requestDestination('/foo/bar/bla/bla/blaaaaaaaa.css')
        requestDestination('/foo/bar/bla/bla/blaaaaaaaa.woff')
        requestDestination('/foo/bar/bla/bla/blaaaaaaaa.svg')
      })
      .on('cycle', (event) => {
        console.log(String(event.target))
      })
      .on('complete', () => {
        const fastest = benchmark.filter('fastest').map('name').shift()
        t.is(fastest, 'Ugly')
        resolve()
      })
      .run({async: true})
  })
})
