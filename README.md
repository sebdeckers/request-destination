# request-destination 🔮

Guess the fetch API request destination based on a file path.

## Use Case

Useful when generating `Link` `rel=preload` headers for HTTP/2 Server Push and other web performance optimisations.

https://fetch.spec.whatwg.org/#concept-request-destination

## Installation

```
npm install request-destination
```

## Usage

```js
const {requestDestination} = require('request-destination')

requestDestination('/foo.js')   // 'script'
requestDestination('/bar.css')  // 'style'
requestDestination('/bla.woff') // 'font'
requestDestination('/lol.html') // ''
requestDestination('/abc')      // ''
```

## Colophon

Made with ❤️ by [Sebastiaan Deckers](https://twitter.com/sebdeckers).
