const mime = require('mime')

// This is the original implementation.
// Its code is easier to read but runs 3-6x slower.

module.exports._requestDestination = (filepath) => {
  if (filepath.toLowerCase().endsWith('.js')) return 'script'
  if (filepath.toLowerCase().endsWith('.css')) return 'style'
  if (filepath.toLowerCase().endsWith('.vtt')) return 'track'

  const type = mime.getType(filepath)

  if (type.startsWith('image')) return 'image'
  if (type.startsWith('audio')) return 'audio'
  if (type.startsWith('video')) return 'video'
  if (type.includes('font')) return 'font'

  return ''
}
